﻿using System;

namespace CalculationSizeCertificate.Common
{
    public class DelegateCommand<T> : DelegateCommandBase
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public DelegateCommand(Action<T> execute, Func<T, bool> canExecute)
        {
            if (execute == null || canExecute == null)
                throw new ArgumentNullException(nameof(execute));

            _execute = execute;
            _canExecute = canExecute;
        }

        public DelegateCommand(Action<T> execute) : this(execute, o => true) { }

        public void Execute(T parameter) => _execute(parameter);

        public bool CanExecute(T parament) => _canExecute(parament);

        protected override void Execute(object parameter)
        {
            Execute((T)parameter);
        }

        protected override bool CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }
    }
}