﻿using System;

namespace CalculationSizeCertificate.Common
{
    public class DelegateCommand : DelegateCommandBase
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public DelegateCommand(Action execute) : this(execute, () => true) { }

        public DelegateCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null || canExecute == null)
                throw new ArgumentNullException(nameof(execute));

            _execute = execute;
            _canExecute = canExecute;
        }

        public void Execute()
        {
            _execute();
        }

        public bool CanExecute()
        {
            return _canExecute();
        }

        protected override void Execute(object parameter)
        {
            Execute();
        }

        protected override bool CanExecute(object parameter)
        {
            return CanExecute();
        }
    }
}