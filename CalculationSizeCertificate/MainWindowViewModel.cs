﻿using System.Windows;
using CalculationSizeCertificate.Common;

namespace CalculationSizeCertificate
{
    public class MainWindowViewModel : BindableBase
    {
        private float _width;
        private float _height;
        private string _resultHeight;
        private string _resultWidthForBlock;
        private string _resultWidthForBookend;

        public MainWindowViewModel()
        {
            CloseAppCommand = new DelegateCommand(() => { Application.Current.Shutdown(); });
        }

        public float Width
        {
            get => _width;
            set
            {
                SetProperty(ref _width, value); 
                UpdateValue();
            }
        }

        public float Height
        {
            get => _height;
            set
            {
                SetProperty(ref _height, value);
                UpdateValue();
            }
        }

        public string ResultWidthForBookend
        {
            get => _resultWidthForBookend;
            private set => SetProperty(ref _resultWidthForBookend, value);
        }

        public string ResultHeight
        {
            get => _resultHeight;
            private set => SetProperty(ref _resultHeight, value);
        }

        public string ResultWidthForBlock
        {
            get => _resultWidthForBlock;
            private set => SetProperty(ref _resultWidthForBlock, value);
        }

        public DelegateCommand CloseAppCommand { get; }

        private void UpdateValue()
        {
            // Установка ширины

            float wblock = 0f;
            float wbookend = 0f;

            if (Width > 0)
            {
                wblock = Width + 2f;
                wbookend = Width - 3f * 2f;
            }

            ResultWidthForBlock = $"{(wblock < 0 ? 0 : wblock)} мм";
            ResultWidthForBookend = $"{(wbookend < 0 ? 0 : wbookend)} мм";

            // Установка высоты
            float rh = Height - 3f * 2;
            ResultHeight = $"{(rh < 0 ? 0 : rh)} мм";
        }
    }
}